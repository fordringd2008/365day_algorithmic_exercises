//
//  main.cpp
//  栈_队_队列
//
//  Created by 3irobotics on 2019/2/6.
//  Copyright © 2019 SSC. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

#define MaxSize 100

/**
 队列是循环队列，方便节省内存，不是循环队列的会假溢出
 */
typedef struct SqQueue{
    int *base; // 基地址
    int front, rear; // 头指针，尾指针
} SqQueue;

// 初始化
bool InitQueue(SqQueue &Q)
{
    Q.base = new int[MaxSize];
    if(!Q.base) return false;
    Q.front = Q.rear = 0; // 头指针和尾指针置为0，队列为空
    return true;
}

// 入队，将元素e放到队尾
bool EnQueue(SqQueue &Q, int e)
{
    // 队满的条件是  (Q.rear+1)%MaxSize == Q.front
    if((Q.rear+1)%MaxSize == Q.front)
        return false;
    Q.base[Q.rear] = e;
    Q.rear = (Q.rear + 1) % MaxSize;
    return true;
}

// 出队
bool DeQuequ(SqQueue &Q, int &e)
{
    // 队空的条件 Q.front = Q.rear
    if (Q.front == Q.rear)
        return false;
    e = Q.base[Q.front]; // 出队的元素
    Q.front = (Q.front + 1) % MaxSize; // 头 + 1
    return true;
}

// 取队头，不修改头指针
int GetHead(SqQueue Q)
{
    if (Q.front != Q.rear)
        return Q.base[Q.front];
    return -1;
}

// 元素的个数  (Q.rear - Q.font + MaxSize) % MaxSize
int QueueLength(SqQueue Q)
{
    return (Q.rear-Q.front+MaxSize)%MaxSize;
}


void queue()
{
    cout<< "abd\n" <<endl;
    

    SqQueue Q;
    int n, x;
    InitQueue(Q); // 初始化
//    cin.ignore();
//    cin.clear();
//    cin.sync();
    cout<< "请输入个数" <<endl;
    cin>>n;
    cout<< "依次输入入队" <<endl;
    while (n--) {
        cin>>x;
        EnQueue(Q, x); // 入对
    }
    cout<<endl;
    cout<< "队列内元素个数"<< QueueLength(Q) <<endl;
    cout<< "队头元素"<< GetHead(Q)  <<endl;
    cout<< "元素依次出队" <<endl;
    while (true) {
        if (DeQuequ(Q, x))
            cout<< x <<"\t";
        else
            break;
    }
    cout <<endl;
    cout<< "队列内元素个数\n"<< QueueLength(Q) <<endl;
}

/*
 栈
 后进先出的线性序列
 可以线性存储，也可以链式存储
 线性存储：顺序栈
 链式存储：链栈
 
 */

/**
 动态分配的栈
 */
typedef struct SqStack{
    int *base; // 栈底
    int *top;  // 栈顶
}SqStack;

/*
// 静态数组
typedef struct SqStack{
    ElemType data[MaxSize]; //定长数组
    int top;
}SqStack;
*/

// 初始化
bool InitStack(SqStack &S)
{
    S.base = new int[MaxSize];
    if (!S.base)
        return false;
    S.top = S.base;
    return true;
}

// 压栈
bool Push(SqStack &S, int e)
{
    if (S.top - S.base == MaxSize) // 栈满
        return false;
    *(S.top++) = e;
    return true;
}

bool Pop(SqStack &S, int &e)
{
    if (S.top == S.base) // 栈空
        return false;
    e = *(S.top--); //
    return true;
}

int GetTop(SqStack S)
{
    if (S.top != S.base)
        return *(S.top -1);
    return -1;
}

void stack()
{
    int n, x;
    SqStack S;
    InitStack(S);
    cout<< "请输入元素的个数：" <<endl;
    cin>>n;
    cout<< "请依次输入n个元素：" <<endl;
    while (n--) {
        cin>>x;
        Push(S, x);
    }
    cout<< "元素依次出栈" <<endl;
    while (S.top != S.base) {
        cout<< GetTop(S) <<endl;
        if (Pop(S, x))
        {
            continue;
        }
        else
        {
            cout<< "出栈完成" <<endl;
        }
    }
    cout<< "完成" <<endl;
    

}

int main(int argc, const char * argv[]) {
    
//    queue();
    stack();
    return 0;
}
