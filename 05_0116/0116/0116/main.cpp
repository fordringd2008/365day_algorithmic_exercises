//
//  main.cpp
//  0116
//
//  Created by 3irobotics on 2019/2/6.
//  Copyright © 2019 3irobotics. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;

int nt[100+5];

//   cin >> s  cin函数遇到 空格， table 回车 就会截断
//   getline 获取输入一行

/*
    主串， 子串
 
    字符串可以使用顺序存储，也可以是用链式存储
 
    1，顺序存储
    连续存储字符串，maxsize固定长度
       1> 在 c,c++,java中 通常以 ‘\0’结束， 并且不在字符串长度内
       2> 在0空间存储字符串的长度
 
 
    C++ 字符串风格 1> string类，  2> c风格，使用 char数组+ int 长度组成 结构体
 
 
    2， 链式存储
    如果插入和删除时需要移动大量元素，因此采用链表的方式
 
 
    模式匹配
    子串的定时运算成为串的模式匹配或串匹配
 S, T
    S  主串，正文串
    T  子串，模式
 
    穷举所有S的所有子串，暴力穷举， Brute Force BF
 
    S 长度 n   T 长度 m
 
    模式匹配的空间复杂度
    最好的情况，第一次就发现不等  总的次数 i - 1 + m
 
    概率均等 pi = 1 / (n - m + 1)
    最好情况下，平均时间复杂度 (n + m) / 2，  O(n + m)
 
    最坏的情况下，每次都比对 m次 才发现不等
    m * (n - m + 2)  平均时间复杂度  O(n * m)   n 大于 m
 
 */


int BF(string s, string t, int pos)
{
    int i = pos, j = 1, sum = 0;
    int slen = (int)s.length();
    int tlen = (int)t.length();
    while (i <= slen && j <= tlen) {
        sum++;
        if (s[i - 1] == t[j - 1])
        {
            i++;
            j++;
        }
        else
        {
            i = i-j+2;
            j = 1;
        }
    }
    cout<< "共匹配次数:" << sum << "次" <<endl;
    if (j > tlen)
        return i - tlen;
    return 0;
}



// 求模式串T的next函数值
void get_next(string t)
{
    int j = 1, k = 0;
    nt[1] = 0;
    while (j < t.length()) {
        if (k == 0 || t[j-1] == t[k-1])
            nt[++j] = ++k;
        else
            k = nt[k];
    }
    for (int i = 0; i <= t.length(); i++) {
        cout<< nt[i] << "" <<endl;
    }
}


/**
 KMP算法，
 1，i不回退
 2，j回退的位置
 */

/*
 S  a b a a b a abeca
 T  a b a a b e
 
 关键在找j回退的位置 回退位置的数组 next[j]
 
 next[j]
 > 0 当 j = 1
 > lmax + 1 T' 的相等前缀和后缀的最大长度lmax (T'为匹配时候时，已经匹配了的半个T，比如T是5个字符，匹配到第5个字符的时候失败了，T‘就是前4个字符)
 > 1 没有相等的前缀后缀
 
 */

/*
 找所有前缀和后置的比较，也不能是暴力穷举，使用动态规划递推
 
 假设已经知道了 next[j] = k, T' = t1t2...tj-1，那么T'的相等前缀最大长度为 k-1
 t1t2...tk-1 = tj-k+1 + tj-k+2 .... tj-1
 长度k-1        长度k-1
 求 next[j+1] = ?
 1> tk = tj  那么 next[j+1] = k+1, 即相等前缀和后缀的长度比next[j]多1
 t1t2...tk-1 tk = tj-k+1 + tj-k+2 .... tj-1 tj
 长度 k           长度 k
 ==> next[j + 1] = k+1
 2> tk != tj 则继续回退找 next[k'] = k'',比较 tk'' 与 tj 是否相等，直到1
 
 */

/*
    设S，T串的长度分别为 n, m KMP算法的特点就是 i 不回退， 当 S[i] != T[j] j回退到next[j] 重新开始比较。
    最快情况下会扫描整个S串，其时间复杂度为 O(n)，计算 next[]数组需要扫描整个T串，时间复杂度为O(m)，
    因此总的时间复杂度为 O（n+m）
 
    BF算法最坏情况下，时间复杂读为 O(n * m) ,最后的情况下为 O（n + M），
    KMP算法时间复杂度为 O(n + m).
    所以只有主串和子串有很多部分匹配的情况下， KMP才显得更优越
 
 
 */

int KMP(string s, string t, int pos)
{
    get_next(t);
    int i = pos, j = 1, sum = 0;
    int slen = (int)s.length();
    int tlen = (int)t.length();
    while (i <= slen && j <= tlen)
    {
        sum++;
        if(s[i - 1] == t[j - 1])
        {
            i++;
            j++;
        }
        else
            j = nt[j]; // j回退到nt[j], i 不回退
    }
    cout<< "共匹配次数:" << sum << "次" <<endl;
    if (j > tlen)
        return i - tlen;
    return 0;
}


int main(int argc, const char * argv[]) {
    
//    string s, t;
//    int pos;
//    cin >> s >> t >> pos;
//    cout << BF(s, t, pos) << endl;
    string s = "abaabaabeca";
    string t = "abaabe";
    int pos = 1; // 从第一个开始找
    int a = KMP(s, t, pos);
    cout<< "找到" << a <<endl;
    
    
    return 0;
}
