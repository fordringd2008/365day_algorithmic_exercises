//
//  main.cpp
//  01_125
//
//  Created by DFD on 2018/12/25.
//  Copyright © 2018 DFD. All rights reserved.
//

#include <iostream>
#include <cmath>
#include <ctime>
using namespace std;


/*
 时间复杂度关系
 
 O(1) < O(logn) < O(n) < O(logn) < O(n^2) < O(n^3) < O(2^n) < O(n!) < O(n^n)
 
 
 */


/*
long long fac(int n)
{
    if (n < 0)
        return  -1;
    else if(n == 0 || n == 1)
        return 1;
    else
        return n * fac(n-1);
}



const int n = 1e4;

int sum1(int n)
{
    int sum = 0;
    for(int i = 1; i <= n; i++)
    {
        sum += pow(-1, i); //
    }
    return sum;
}

int sum2(int n)
{
    int sum = 0;
    if(n % 2 == 0)
        sum = 0;
    else
        sum = -1;
    return sum;
}

int main(int argc, const char * argv[]) {
    
    // 316297
    // 422113
    time_t s=0,e,sumtime;
    cout<<"sum:"<<sum2(2)<<endl;
    e=clock();
    sumtime=e-s;
    cout<<"time:"<<sumtime<<endl;
    
    return 0;
}
*/























/*
 
 斐波那契数列
 
 1，1，2，3，5，8，13，21，34
 
 n 项 = (n-1)项 + (n-2)项
 
         1                  ,n = 1
 F(n) =  1                  ,n = 2
         F(n-1) + F(n-2)    ,n > 2
 
 
 F(n-1) / F(n) 趋近于 0.618
 
 

// 指数阶
int Fib1(int n)
{
    if(n < 1)
        return -1;
    if(n == 1 || n == 2)
        return 1;
    return Fib1(n - 1) + Fib1(n - 2);
}

// 多项式阶，时间复杂度O(n),空间复杂度O(n)
// 每次都要记录之前的项
int Fib2(int n)
{
    if(n < 1)
        return -1;
    int *a = new int[n+1];
    a[1] = 1;
    a[2] = 1;
    for(int i = 3; i <= n; i++)
        a[i] = a[i-1] + a[i-2];
    return a[n];
}

// 时间复杂度 O(n), 空间复杂度 O(1)
int Fib3(int n)
{
    int i=0, s1, s2;
    if(n < 1)
        return -1;
    if (n == 1 || n == 2)
        return 1;
    s1 = 1;
    s2 = 1;
    for (i = 3; i <= n; i++) {
        s2 = s1 + s2;
        s1 = s2 - s1;
    }
    return s2;
}


int main()
{
    cout<<Fib3(9)<<endl;
}
*/


/*
    马克思手稿中的数学题
    30个人，有男人，女人，小孩，每个男人花3先令，每个女人花2先令，
    每个小孩花1先令，共花了50先令，问各有几个男人，女人，小孩
 
    x + y + z = 50
    3x + 2y + z = 50
 
    2x + y = 20
 
 https://www.luogu.rog/problemnew/lists?name=&orderitem=difficulty&tag=&content=0&type=
 
 

int main()
{
    int x,y,z,count=0;
    cout<<" Men, Women, Children"<<endl;
    for(x = 1; x <= 9; x++)
    {
        y = 20 - 2 * x;
        z = 30 - x - y;
        if (3 * x + 2 * y + z == 50)
        {
            //count++;
            cout<<x<<endl;
            cout<<y<<endl;
            cout<<z<<endl;
        }
        cout<<"---------"<<endl;
    }
    
    return 0;
    
}
*/
/*
 两个整数a,b，若他们除以整数m所得的余数相等，
 则成a,b对于模m同余，记作a=b(mod m),读作 a同余于b模m

 n = 1(mod2)
 n = 2(mod3)
 n = 4(mod5)
 n = 5(mod6)
 n = 0(mod7)
 
 


int main1()
{
    int n = 1;
    while (!((n % 2 == 1) && (n % 3 == 2) && (n % 5 == 4) && (n % 6 == 5) && (n % 7 == 0))) {
        n++;
    }
    cout<<n<<endl;
    return 0;
}


int main()
{
    int n = 7;
    while (!((n % 2 == 1) && (n % 3 == 2) && (n % 5 == 4) && (n % 6 == 5) && (n % 7 == 0))) {
        n+=7;
    }
    cout<<n<<endl;
}
*/
// 还可以考虑 2，3，5，6的最小公倍数n, 然后 t= n-1,再判断t= 0(mode 7)


/*
 哥德巴赫猜想， 任一大于2的偶数，都可以是表示成两个素数之和
 */

int prime(int i); // 判断是否为素数

int main()
{
    int i, n;
    for (i = 4; i < 2000; i+=2) {
        for (n = 2; n < i; n++) {
            if (prime(n))
            {
                if (prime(i -n))
                {
                    cout<< i <<"=" << n << "+" << i - n <<endl;
                    break;
                }
            }
            if (n == i)
            {
                cout<<"error"<<endl;
            }
        }
    }
}

int prime(int i)
{
    int j;
    if (i <= 1) return 0;
    if (i == 2) return 1;
    for (j = 2; j<=(int)(sqrt(i)); j++) {
        if (!(i%j)) return 0;
    }
    return 1;
}





