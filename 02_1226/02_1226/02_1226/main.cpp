//
//  main.cpp
//  02_1226
//
//  Created by DFD on 2019/1/13.
//  Copyright © 2019 DFD. All rights reserved.
//

/*
 
 C++ 点调用和->调用
 
 .调用  使用变量名调用
 ->  指针调用使用 -> 调用
 
 struct Stu{
    int age;
    string *name;
    Stu *net;
 }
 
 Stu stu = new Stu()
 stu.age
 stu.net -> name
 
 
 指针是指针变量的简称，里面放的是一个地址
 
 int * a;
 
 一个int指针类型的指针，里面放的是一个int变量的地址
 
 没有标记的一端，先修改
 
 链表的头指针不可以随意改动
 
 查找链表中第i个节点
 
 X = P -> next()
 X = P
 
 
 */


#include <iostream>
#include <string>
//#include <iomainip>
#include <stdlib.h>

using namespace std;

// 声明一个结构体，并给他指明为 LNode, LinkList*类型的
typedef struct LNode{
    int data;
    struct LNode *next;
}LNode, *LinkList;

bool InitList_L(LinkList &L) // 构造一个空的单向列表
{
    L = new LNode();
    if(!L)
    {
        return false;
    }
    L->next = NULL;
    return true;
}


/* 前插法，相当于压栈，先把1插入到0后面，然后把2插入到0和1之间，然后把3插入到0和2之间
 每次都插入在L的头部
 0，1
 0，2，1
 0，3，2，1
 0，4，2，1
 ...
*/

void Print(LinkList &L)
{
    cout<< "开始验证" <<endl;
    LinkList t = L;
    while (t->next)
    {
        LNode *node = t->next;
        cout<< node->data <<endl;
        t = t->next;
    }
    printf("结束验证");
}

void CreateList_H(LinkList &L)
{
    int n;
    LinkList s;
    L = new LNode;
    L ->next = NULL;
    cout<<"请输入元素个数n:" <<endl;
    cin>>n;
    cout<< "请依次输入n个元素" <<endl;
    cout<< "前插法创建单链表" <<endl;
    while (n--)
    {
        s = new LNode;  // 生成新节点
        cin>>s->data;   // 赋值给新节点的数据域
                           // 先赋值没有标记的一段
        s->next = L->next; // 把新节点的next域赋值为原来L的next
        L->next = s;       // 把原来L的next域指向新节点
    }
//    Print(L);
}

/*
 尾插法创建单向列表
 */
void CreateList_R(LinkList &L)
{
    
    L = new LNode;
    L->next = NULL;
    LinkList s, r;
    r = L;
    int n = 0;  // 个数
    cout<< "请输入元素个数n:" <<endl;
    cin>>n;
    cout<< "依次输入n个元素" <<endl;
    while (n--) {
        s = new LNode;
        cin>>s->data;
        s->next = NULL;
        r->next = s;
        r = s;
    }
    
//    Print(L);
}

/**
 单列表取值

 @param i 索引
 @param e 值
 @return 是否超出范围
 */
bool GetElem_L(LinkList L, int i, int &e)
{
    int j = 0;
    LinkList next = L;
    while (j <= i) {
        next = next->next;
        if (next != NULL)
        {
            if (j == i)
            {
                e = next->data;
                return true;
            }else {
                j ++;
            }
        }else{
            return false;
        }
    }
    return true;
}

void CreateRadomLinkList(LinkList &L, int begin, int num, int step)
{
    L = new LNode;
    L->next = NULL;
    LinkList r, s;
    r = L;
    int last = begin + num * step;
    for (int i = begin; i < last; i += step)
    {
        s = new LNode;
        s->data = i;
        s->next = NULL;
        r->next = s;
        r = s;
    }
}

/**
 单列表的插入

 @param L 列表
 @param i 索引
 @param e 元素
 @return 是否成功
 */
bool ListInsert_L(LinkList &L, int i, int e)
{
    LinkList s = L->next;
    LinkList r = new LNode;
    for (int j = 0; j <= i && s != NULL; j++)
    {
        if(j != i - 1)
        {
            s = s -> next;
        } else {
            r -> next = s -> next;
            r -> data = e;
            s -> next = r;
            return true;
        }
    }
    return false;
}

/**
 单列表的删除

 @param L 单列表
 @param i 索引
 @return 是否成功
 */
bool ListDelete_L(LinkList &L, int i)
{
    LinkList s = L->next;
    for(int j = 0; j < i && s != NULL; j++)
    {
        if (j < i - 1)
        {
            s = s -> next;
        }
        if (j == i-1)
        {
            LinkList r = s -> next;
            if (r == NULL){
                s -> next = NULL;
            } else {
                // 内存泄露,删除的那个节点要释放掉
                LinkList d = r;
                s -> next = r -> next;
                delete d;
                return true;
            }
        }
    }
    return false;
}


/**
 合并两个有序非递减单列表(增加，非递增)

 @param la 列表1
 @param lb 列表2
 @param Lc 新列表
 */
void mergelinkList(LinkList la, LinkList lb, LinkList &Lc)
{
//    Lc = la->next;
//    LinkList s = la->next;
//    LinkList t = lb->next;
//    LinkList r;
//    while (s != NULL && t != NULL) {
//        r->next = s -> data < t -> data ? s: t;
//        s -> data
//        
//        
//    }
    
    
    
    
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
    LinkList L;
    LinkList M;
    
//    CreateList_H(L);
//    CreateList_R(L);
//
//    int e = 0;
//    bool result = GetElem_L(L, 7, e);
//    cout<< result <<endl;
//    cout<< e <<endl;

    
    CreateRadomLinkList(L, 4, 10, 3);
    cout<< "L:" <<endl;
    Print(L);
    
    
    CreateRadomLinkList(M, 5, 6, 2);
    cout<< "L:" <<endl;
    Print(M);
    
    LinkList Lc;
    mergelinkList(L, M, Lc);
    Print(Lc);
    
    
    
    
//    CreateRadomLinkList(<#LinkList &L#>)
    
    //ListInsert_L(L, 10, 5);
    
//    ListDelete_L(L, 1);
    
//    Print(L);
    
    return 0;
}
